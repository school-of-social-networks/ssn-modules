<?php
/*
@package ssn-modules
*/
/*
Plugin Name: Ssn Modules
Description: Achievements card system.
Version: 1.0.0
Author: Kushuh
Author URI: https://github.com/Kushuh
License: MIT
Text Domain: ssn-modules
*/

define( 'PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once PLUGIN_DIR . 'guard.php';
require_once PLUGIN_DIR . 'ext-templates/elementor/ssn-units-elementor.php';

// Disable access from external source.
check_wp_env();

class SsnModules
{
    public function __construct()
    {

    }

    public function activate()
    {
        flush_rewrite_rules();

        global $wpdb;

        $table_name_1 = $wpdb->prefix . "ssn_modules";
        $table_name_2 = $wpdb->prefix . "ssn_bot_messages";
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name_1 (
            id INT NOT NULL,
            counter BIGINT NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;
        CREATE TABLE $table_name_2 (
            id INT NOT NULL,
            counter BIGINT NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }

    public function deactivate()
    {
        flush_rewrite_rules();
    }

    public static function uninstall()
    {
        global $wpdb;

        $table_name = $wpdb->prefix . "ssn_modules";
        $wpdb->query( "DROP TABLE IF EXISTS $table_name" );

        $table_name = $wpdb->prefix . "ssn_bot_messages";
        $wpdb->query( "DROP TABLE IF EXISTS $table_name" );
    }

    public function register_scripts()
    {
        $categories = get_categories(
            array( 'child_of' => 88, 'numberposts' => -1 )
        );

        $children_modules = array();

        if ( $categories )
        {
            foreach( $categories as $category )
            {
                $posts = get_posts(
                    array( 'category' => $category->term_id, 'numberposts' => -1 )
                );

                $ids = array();
                if ( $posts )
                {
                    foreach ( $posts as $post )
                    {
                        array_push($ids, $post->ID);
                    }
                }

                $children_modules[$category->term_id] = $ids;
            }
        }

        wp_register_script(
            'ssn-svg-generator',
            plugin_dir_url( __FILE__ ) . 'js/svg-generator.js',
            array(),
            '1.0.1',
            false
        );

        wp_enqueue_script( 'ssn-svg-generator' );

        wp_register_script(
            'ssn-global-utils',
            plugin_dir_url( __FILE__ ) . 'js/global-utils.js',
            array(),
            '1.0.0',
            false
        );

        wp_enqueue_script( 'ssn-global-utils' );

        wp_register_script(
            'ssn-module-global',
            plugin_dir_url( __FILE__ ) . 'js/ssn-module-global.js',
            array(),
            '1.0.21',
            false
        );

        wp_enqueue_script( 'ssn-module-global' );

        wp_register_script(
            'ssn-modules-interaction',
            plugin_dir_url( __FILE__ ) . 'js/ssn-card-handler.js',
            array(),
            '1.0.88',
            false
        );

        wp_enqueue_script( 'ssn-modules-interaction' );

        wp_localize_script( 'ssn-modules-interaction', 'ssn',
            array(
                'childrenModules' => json_encode($children_modules),
                'incrementCounterURL' => content_url( '/plugins/ssn-modules/standalone/increment_global_count.php' ),
                'fetchCounterURL' => content_url( '/plugins/ssn-modules/standalone/fetch_global_count.php' ),
                'fetchBotnationCounterURL' => content_url( '/plugins/ssn-modules/standalone/fetch_bn_count.php' ),
                'fetchPostByCategories' => content_url( '/plugins/ssn-modules/standalone/fetch_posts.php' )
            )
        );

        wp_register_script(
            'ssn-bind-controls',
            plugin_dir_url( __FILE__ ) . 'js/bind-controls.js',
            array(),
            '1.0.6',
            false
        );

        wp_enqueue_script( 'ssn-bind-controls' );
    }

    public function register()
    {
        $this->register_scripts();

        // activation.
        register_activation_hook( __FILE__, array( $this, 'activate' ) );
        // deactivation.
        register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
        // uninstall.
        register_uninstall_hook( __FILE__, array( 'SsnModules', 'uninstall' ) );
    }
}

$cards = new SsnModules();
$cards->register();