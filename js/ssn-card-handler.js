ssn = ssn || {childrenModules: '{}', incrementCounterURL: '', fetchCounterURL: '', fetchBotnationCounterURL: '', fetchPostByCategories: ''};

const SSN_parseCard = el => ({
    id: el.dataset.cardId,
    modules: JSON.parse(el.dataset.cardModules),
    tags: JSON.parse(el.dataset.cardTags).map(({term_id}) => term_id)
});

const SSN_getOverallProgress = () => {
    return JSON.parse(ssn.childrenModules);
};

const SSN_getAndUpdateProgress = (filter, available) => {
    const progress = SSN_filterCards(filter);

    if (available == null) {
        const overall = SSN_getOverallProgress();
        if (filter.modules != null && filter.modules.length > 0) {
            available = [];
            for (const module of filter.modules) {
                available.push(...(overall[module] || []))
            }
        } else {
            available = Object.values(overall).reduce((acc, vals) => [...acc, ...vals], []);
        }
    }

    return {
        progress: progress || [],
        overall: available
    };
};

const SSN_fetcher = async (url, params) => {
    const res = await fetch(
        url,
        params
    );

    const tres = await res.text();

    try {
        const jres = JSON.parse(tres);
        return jres;
    } catch (e) {
        console.error(e);
        console.error(tres);

        return null;
    }
};

const SSN_updateCounter = async category => {
    const res = await SSN_fetcher(
        ssn.incrementCounterURL,
        {
            method: 'POST',
            body: JSON.stringify({id: category})
        }
    );

    if (res.error) {
        console.error(res.error);
    }
};

const SSN_getChatbotCount = async () => {
    const res = await SSN_fetcher(
        ssn.fetchBotnationCounterURL,
        {method: 'POST'}
    );

    if (res == null) {
        console.error('unable to fetch results.');
        return;
    }

    if (res.results == null) {
        return 0;
    }

    return Object.values(res.results).reduce((acc, count) => acc + parseInt(`${count}`, 10), 0);
};

const SSN_getCount = async (...categories) => {
    const res = await SSN_fetcher(
        ssn.fetchCounterURL,
        {
            method: 'POST',
            body: categories.length ? `{"ids": "${categories.join(', ')}"}` : '{}'
        }
    );

    if (res == null) {
        console.error('unable to fetch results.');
        return;
    }

    if (res.results == null) {
        return 0;
    }

    return Object.values(res.results).reduce((acc, count) => acc + parseInt(`${count}`, 10), 0);
};

const SSN_getLocalCards = () => {
    const tl = localStorage.getItem('ssn-module-cards');
    if (tl == null) {
        return [];
    }

    try {
        return JSON.parse(tl);
    } catch {
        localStorage.setItem('ssn-module-cards', '[]');
        return [];
    }
};

const SSN_filterCards = filter => {
    const local = SSN_getLocalCards();
    if (local.length === 0 || filter == null) {
        return local;
    } else {
        return local.filter(
            x => {
                if (filter.id != null && filter.id.length > 0 && x.id !== filter.id) {
                    return false;
                }

                let flag = true;
                if (filter.modules != null && filter.modules.length > 0) {
                    flag = false;
                    for (const module of filter.modules) {
                        if (x.modules.find(y => `${y}` === module)) {
                            flag = true;
                            break;
                        }
                    }
                }

                if (filter.tags != null && filter.tags.length > 0) {
                    flag = false;
                    for (const tag of filter.tags) {
                        if (x.tags.find(t => `${t}` === tag)) {
                            flag = true;
                            break;
                        }
                    }
                }

                return flag;
            }
        );
    }
};

const SSN_removeCards = (...ids) => {
    const cards = SSN_getLocalCards();
    localStorage.setItem(
        'ssn-module-cards',
        JSON.stringify(
            cards.filter(
                x => !ids.find(y => y === x.id)
            )
        )
    );
};

const SSN_filterAndRestrict = filter => {
    const cards = SSN_filterCards(filter);
    if (filter.available != null) {
        SSN_removeCards(cards.filter(x => !filter.available.find(y => y === x.id)));
        return cards.filter(x => filter.available.find(y => y === x.id));
    }

    return cards;
};

const SSN_insertCard = card => {
    const cards = SSN_getLocalCards().filter(x => x.id !== card.id);
    cards.push(card);
    localStorage.setItem('ssn-module-cards', JSON.stringify(cards));

    SSN_updateCounter('global').catch(console.error);

    document.dispatchEvent(
        new CustomEvent(
            'ssn-module-update',
            {detail: card}
        )
    );

    return cards;
};
