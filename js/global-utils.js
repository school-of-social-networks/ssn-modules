/**
 * Visual handler.
 *
 * @param el - the button element to rotate.
 * @returns {boolean}
 */
const rotateExpandButton = el => {
    const isExpanded = el.classList.contains('ssn__unit-expand-button-expanded');

    // Toggle value.
    if (isExpanded) {
        el.classList.remove('ssn__unit-expand-button-expanded');
    } else {
        el.classList.add('ssn__unit-expand-button-expanded');
    }

    // We toggled the value so it has now changed.
    return !isExpanded;
};