const SSN_Module_getDefaultElements = th => ({
    $content: th.$element.find( '.ssn__unit-content' ),
    $units: th.$element.find( '.ssn__unit-element' ),
    $cards: th.$element.find( '.ssn__unit-card-preview' ),
    $unitCards: th.$element.find( '.ssn__unit-card-view' )
});

const SSN_Module_updateProgress = (th, filter) => e => {
    // Event can be triggered with no cardName to update component visual state without requiring
    // local storage access.
    if (e.detail) {
        // Save cardName in unlocked cards tracker, if not already present.
        SSN_insertCard(e.detail);
    }

    // progress holds a list of unlocked cards.
    const progress = SSN_filterAndRestrict({
        ...filter,
        available: [...th.elements.$cards].map( card => card.dataset.cardId )
    });

    // Update preview cards status with special style, if unlocked.
    for (const card of [...th.elements.$cards]) {
        const pc = SSN_parseCard(card);
        if (progress.find(x => x.id === pc.id)) {
            card.classList.add('ssn__unit-card-preview-unlocked');
        } else {
            card.classList.remove('ssn__unit-card-preview-unlocked');
        }
    }

    return progress;
};

const SSN_Module_updateRows = th => {
    let switcher = false;
    for (const el of th.elements.$units) {
        if (!el.classList.contains('ssn__unit-element-opened')) {
            el.classList.remove('ssn__unit-element-odd');
            continue;
        }

        if (switcher) {
            el.classList.add('ssn__unit-element-odd');
        } else {
            el.classList.remove('ssn__unit-element-odd');
        }

        switcher = !switcher;
    }
};

const SSN_Module_loadContent = th => {
    for (const unit of th.elements.$units) {
        fetch(
            unit.dataset.unitUrl,
            {method: "GET"}
        )
            .then(
                async result => {
                    unit.querySelector('.ssn__unit-lessons').innerHTML = await result.text();
                }
            )
            .catch(console.error);
    }
};

const SSN_Module_bindEvents = th => {
    const header = document.querySelector('#show-hide-header');
    const headerHeight = header == null ? 0 : header.getBoundingClientRect().height;

    for (const card of [...th.elements.$cards]) {
        card.addEventListener(
            'click',
            () => {
                card.classList.add('ssn__unit-card-preview-opened');

                setTimeout(
                    () => {
                        const fullView = th.$element[0].querySelector(
                            `[data-unit-id="${card.dataset.cardId}"]`
                        );

                        fullView.style.height = 'unset';
                        fullView.classList.remove('ssn__unit-element-hidden');
                        fullView.classList.add('ssn__unit-element-opened');

                        th.updateRows.bind(th)();
                        th.updateProgress({detail: SSN_parseCard(card)});
                        setTimeout(
                            () => {
                                window.scrollBy(0, fullView.getBoundingClientRect().top - headerHeight - 10);
                            },
                            650
                        );
                    },
                    300
                );
            }
        );
    }

    for (const card of [...th.elements.$unitCards]) {
        card.addEventListener(
            'click',
            () => {
                const parent = card.parentNode;

                parent.classList.add('ssn__unit-element-hidden');
                parent.classList.remove('ssn__unit-element-opened');

                const preview = th.$element[0].querySelectorAll(
                    `[data-card-id="${parent.dataset.unitId}"]`
                );

                for (const p of preview) {
                    p.classList.remove('ssn__unit-card-preview-opened');
                }

                setTimeout(
                    () => {
                        parent.style.height = '0';
                        th.updateRows.bind(th)();
                    },
                    100
                );
            }
        );
    }
};