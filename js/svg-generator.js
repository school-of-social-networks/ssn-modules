const roundTo = (num, dec = 2) => (Math.round(num) * Math.pow(10, dec)) / Math.pow(10, dec);

const polarToCartesian = (centerX, centerY, radius, angleInDegrees) => {
    const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;

    return {
        x: roundTo(centerX + (radius * Math.cos(angleInRadians))),
        y: roundTo(centerY + (radius * Math.sin(angleInRadians)))
    };
};

const describeArc = (x, y, radius, startAngle, endAngle) => {
    if (endAngle - startAngle === 360) {
        return `${describeArc(x, y, radius, startAngle, endAngle - 5)} ${describeArc(x, y, radius, endAngle - 10, endAngle)}`;
    }

    x = parseInt(x, 10);
    y = parseInt(y, 10);
    radius = parseInt(radius, 10);
    startAngle = parseInt(startAngle, 10);
    endAngle = parseInt(endAngle, 10);

    const start = polarToCartesian(x, y, radius, endAngle);
    const end = polarToCartesian(x, y, radius, startAngle);

    const largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

    return `M ${start.x} ${start.y} A ${radius} ${radius} 0 ${largeArcFlag} 0 ${end.x} ${end.y}`;
};

const generateArcs = (unlocked, total, {color, backgroundColor, x, y, r, ...strokeParams} = {}) => {
    if (unlocked === 0 || total === 0) {
        return [
            [
                'path',
                {
                    fill: 'none',
                    d: describeArc(x, y, r, 0, 360),
                    stroke: backgroundColor,
                    ...(strokeParams || {})
                }
            ]
        ];
    }

    if (unlocked === total) {
        return [
            [
                'path',
                {
                    fill: 'none',
                    d: describeArc(x, y, r, 0, 360),
                    stroke: color,
                    ...(strokeParams || {})
                }
            ]
        ];
    }

    return [
        [
            'path',
            {
                fill: 'none',
                d: describeArc(x, y, r, 0, Math.ceil((unlocked / total) * 360)),
                stroke: color,
                ...(strokeParams || {})
            }
        ],
        [
            'path',
            {
                fill: 'none',
                d: describeArc(x, y, r, Math.floor((unlocked / total) * 360), 360),
                stroke: backgroundColor,
                ...(strokeParams || {})
            }
        ]
    ];
};


const buildTag = (tagName, {children, ...tagParams} = {}, close = false) =>
    `<${tagName} ${Object.entries(tagParams).reduce(
        (acc, [k, v]) => v != null ? `${acc} ${k}="${v}"` : acc,
        ''
    )}>
        ${children != null && children.constructor.name === 'Array' ?
            children.reduce(
                (acc, [t, s]) => `${acc} ${buildTag(t, s, true)}`,
                ''
            ) : ''
        }
    ${close ? `</${tagName}>` : ''}`;

const generateSvg = (
    {
        width,
        height,
        encoding = 'utf-8',
        id,
        x = '0px',
        y = '0px',
        xmlSpace = 'preserve',
        struct
    }
) => {
    const xml = `<?xml version="1.0" encoding="${encoding}"?>`;
    const svgTag = buildTag('svg', {
        version: '1.1',
        xmlns: 'http://www.w3.org/2000/svg',
        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
        id,
        x,
        y,
        viewBox: `0 0 ${width} ${height}`,
        style: `enable-background:new 0 0 ${width} ${height};`,
        'xml:space': xmlSpace,
        children: struct
    }, true);

    return xml + svgTag;
};