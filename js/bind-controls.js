const SSN_resetGame = e => {
    e.preventDefault();
    resetCtx();
    localStorage.removeItem('ssn-module-cards');
};

const SSN_openChatbot = e => {
    e.preventDefault();
    window.BNAI.openChat();
};

window.onload = () => {
    const resetButton = document.querySelectorAll('#reset-game');
    for (const button of resetButton) {
        button.addEventListener('click', SSN_resetGame);
        button.style.cursor = 'pointer!important';
    }

    const chatbotButton = document.querySelectorAll('#open-chatbot');
    for (const button of chatbotButton) {
        button.addEventListener('click', SSN_openChatbot);
        button.style.cursor = 'pointer!important';
    }
};