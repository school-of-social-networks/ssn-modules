<?php
define( 'SHORTINIT', true );
$path = preg_replace('/wp-content(?!.*wp-content).*/','',__DIR__);
include($path.'wp-load.php');

$POST = json_decode(file_get_contents('php://input'), true);
$response = array();

global $wpdb;
$wpdb->show_errors( true );
$table_name = $wpdb->prefix . "ssn_modules";

$sqlWhere = "";

if ( isset( $POST['ids'] ) )
{
    $sqlWhere = "WHERE (id) IN (%s)";
}

$sql = "SELECT * FROM $table_name $sqlWhere";

$counters = $wpdb->get_results( $wpdb->prepare( $sql, $POST['ids'] ) );

if ( $counters )
{
    $response['results'] = array();
    foreach ( $counters as $counter )
    {
        $response['results'][$counter->id] = $counter->counter;
    }
}

$response['query'] = $sql;
$response['counters'] = $counters;

echo json_encode($response);