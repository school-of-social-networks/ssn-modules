<?php
$path = preg_replace('/wp-content(?!.*wp-content).*/','',__DIR__);
include($path.'wp-load.php');

$POST = json_decode(file_get_contents('php://input'), true);
$response = array();

if ( !isset( $POST['category'] ) && !isset( $POST['tag'] ) && !isset( $POST['tag_id'] ) )
{
    $response['status'] = "0";
    $response['error'] = "no category or tag provided";
    echo json_encode($response);
}

function parse_posts( $posts )
{
    $output = array();

    if ( $posts )
    {
        foreach ( $posts as $post )
        {
            $post_title = $post->post_title;
            $url = get_permalink( $post->ID );
            $categories = wp_get_post_categories( $post->ID );

            array_push(
                $output,
                array(
                    'id' => $post->ID,
                    'title' => $post_title,
                    'url' => $url,
                    'modules' => $categories,
                    'tags' => wp_get_post_tags( $post->ID )
                )
            );
        }
    }

    return $output;
}

$selector = array( 'numberposts' => -1, 'orderby' => array( 'post_date' => 'ASC' ) );

if ( isset( $POST['category'] ) )
{
    $selector['category'] = $POST['category'];
}

if ( isset( $POST['tag'] ) )
{
    $selector['tag_id'] = $POST['tag'];
}

if ( isset( $POST['tag_id'] ) )
{
    $selector['tag_id'] = $POST['tag_id'];
}

$posts = get_posts( $selector );

echo json_encode( parse_posts( $posts ) );
