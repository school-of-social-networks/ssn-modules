<?php
define( 'SHORTINIT', true );
$path = preg_replace('/wp-content(?!.*wp-content).*/','',__DIR__);
include($path.'wp-load.php');

$POST = json_decode(file_get_contents('php://input'), true);
$response = array();

if ( !isset( $POST['id'] ) )
{
    $response['status'] = "0";
    $response['error'] = "no id provided";
    echo json_encode($response);
}

global $wpdb;
$table_name = $wpdb->prefix . "ssn_modules";

$sql = "INSERT INTO $table_name (id, counter) VALUES (%s, 1) ON DUPLICATE KEY UPDATE counter = counter + 1";

$wpdb->query(
    $wpdb->prepare($sql, $POST['id'])
);

$table_name = $wpdb->prefix . "ssn_bot_messages";

$sql = "INSERT INTO $table_name (id, counter) VALUES ('global', 1) ON DUPLICATE KEY UPDATE counter = counter + %d";

$inc = 0;

try {
    $inc = random_int(0, 53);
} catch (Exception $e) {

}

$wpdb->query(
    $wpdb->prepare($sql, $inc)
);

$response['status'] = 1;
echo json_encode($response);