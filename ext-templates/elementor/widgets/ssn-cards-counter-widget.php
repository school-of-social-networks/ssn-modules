<?php

namespace Ssn\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Utils;

class SsnCounter extends Widget_Base
{
    public function __construct($data = [], $args = null) {
        parent::__construct($data, $args);

        wp_register_script(
            'ssn-counters-reactive-script',
            plugins_url() . '/ssn-modules/ext-templates/elementor/scripts/ssn-counters-reactive-script.js',
            [ 'elementor-frontend' ],
            '1.0.88',
            true
        );
    }

    public function get_name()
    {
        return 'ssn-counter';
    }

    public function get_title()
    {
        return 'SSN Counter';
    }

    public function get_icon()
    {
        return 'fas fa-award';
    }

    public function get_categories()
    {
        return [ 'general' ];
    }

    public function get_style_depends()
    {
        return [ 'ssn-counter-style' ];
    }

    public function get_script_depends()
    {
        return [ 'ssn-counters-reactive-script' ];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'configuration',
            [
                'label' => __( 'Configuration', 'ssn' )
            ]
        );

        $this->add_control(
            'counter-label',
            [
                'label' => 'Label',
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'placeholder' => __( 'My counter label', 'ssn' )
            ]
        );

        $modules = new Repeater();

        $modules->add_control(
            'sub_module_id',
            [
                'label' => __( 'Sub-Module ID', 'ssn' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'placeholder' => __( 'my-module-id', 'ssn' )
            ]
        );

        $this->add_control(
            'sub-modules',
            [
                'label' => __( 'Sub-Modules', 'ssn' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $modules->get_controls(),
                'default' => [
                    [
                        'sub_module_id' => ''
                    ]
                ],
                'title_field' => '{{{ sub_module_id }}}'
            ]
        );

        $this->add_control(
            'show_icon',
            [
                'label' => __( 'Show icon', 'ssn' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'ssn' ),
                'label_off' => __( 'Hide', 'ssn' ),
                'default' => ''
            ]
        );

        $this->add_control(
            'show_circular_counter',
            [
                'label' => __( 'Show wheel counter', 'ssn' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'ssn' ),
                'label_off' => __( 'Hide', 'ssn' ),
                'default' => ''
            ]
        );

        $this->add_control(
            'inline_mode',
            [
                'label' => __( 'Inline mode', 'ssn' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes', 'ssn' ),
                'label_off' => __( 'No', 'ssn' ),
                'default' => ''
            ]
        );

        $this->add_control(
            'show_total_score',
            [
                'label' => __( 'Show total', 'ssn' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Show', 'ssn' ),
                'label_off' => __( 'Hide', 'ssn' ),
                'default' => ''
            ]
        );

        $this->add_control(
            'is_global',
            [
                'label' => __( 'Global counter', 'ssn' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes', 'ssn' ),
                'label_off' => __( 'No', 'ssn' ),
                'default' => ''
            ]
        );

        $this->add_control(
            'is_botnation',
            [
                'label' => __( 'Botnation counter', 'ssn' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __( 'Yes', 'ssn' ),
                'label_off' => __( 'No', 'ssn' ),
                'default' => ''
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'theme',
            [
                'label' => __( 'Theme', 'ssn-modules' )
            ]
        );

        $this->add_control(
            'theme-color',
            [
                'label' => 'Color',
                'type' => Controls_Manager::COLOR,
                'default' => '#323232'
            ]
        );

        $this->add_control(
            'theme-color-completed',
            [
                'label' => 'Color (completed)',
                'type' => Controls_Manager::COLOR,
                'default' => '#6C63FF'
            ]
        );

        $this->add_control(
            'theme-label-font-size',
            [
                'label' => 'Font size (rem)',
                'type' => Controls_Manager::NUMBER,
                'default' => '1',
                'min' => 0
            ]
        );

        $this->add_control(
            'theme-icon-spacing',
            [
                'label' => 'Icon spacing (em)',
                'type' => Controls_Manager::NUMBER,
                'default' => '0.8',
                'min' => 0
            ]
        );

        $this->add_control(
            'theme-label-weight',
            [
                'label' => 'Font weight',
                'type' => Controls_Manager::NUMBER,
                'default' => '500',
                'min' => 100,
                'max' => 1000,
                'step' => 50
            ]
        );

        $this->add_control(
            'theme-wheel-bg-color',
            [
                'label' => 'Wheel background color',
                'type' => Controls_Manager::COLOR,
                'default' => '#323232'
            ]
        );

        $this->add_control(
            'theme-wheel-fill-color',
            [
                'label' => 'Wheel fill color',
                'type' => Controls_Manager::COLOR,
                'default' => '#6C63FF'
            ]
        );

        $this->add_control(
            'theme-icon-size',
            [
                'label' => 'Icon size (rem)',
                'type' => Controls_Manager::NUMBER,
                'default' => '4',
                'min' => 0
            ]
        );

        $this->add_control(
            'theme-wheel-size',
            [
                'label' => 'Wheel diameter (rem)',
                'type' => Controls_Manager::NUMBER,
                'default' => '5',
                'min' => 0
            ]
        );

        $this->add_control(
            'theme-wheel-thickness',
            [
                'label' => 'Wheel tickness (rem)',
                'type' => Controls_Manager::NUMBER,
                'default' => '1',
                'min' => 0
            ]
        );

        $this->add_control(
            'animation-duration',
            [
                'label' => 'Animation duration (ms)',
                'type' => Controls_Manager::NUMBER,
                'default' => 1000,
                'min' => 0
            ]
        );

        $this->add_control(
            'counter-icon',
            [
                'label' => __( 'Counter icon', 'elementor-pro' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ]
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_render_attribute(
            'counter-wrapper',
            [
                'class' => [
                    'ssn__counter',
                    $settings['inline_mode'] == 'yes' ? 'ssn__counter-inline' : ''
                ],
                'style' =>
                    '--theme-color: ' . $settings['theme-color'] . ';' .
                    '--theme-color-completed: ' . $settings['theme-color-completed'] . ';' .
                    '--theme-label-font-size: ' . $settings['theme-label-font-size'] . 'rem;' .
                    '--theme-label-weight: ' . $settings['theme-label-weight'] . ';' .
                    '--theme-icon-size: ' . $settings['theme-icon-size'] . 'rem;' .
                    '--theme-icon-spacing: ' . $settings['theme-icon-spacing'] . 'em;' .
                    '--theme-wheel-size: ' . $settings['theme-wheel-size'] . 'rem;' .
                    '--theme-wheel-bg-color: ' . $settings['theme-wheel-bg-color'] . ';' .
                    '--theme-wheel-fill-color: ' . $settings['theme-wheel-fill-color'] . ';' .
                    '--theme-wheel-thickness: ' . $settings['theme-wheel-thickness'] . 'rem;'
                ,
                'data-linked-units' => json_encode( $settings['sub-modules'] ),
                'data-animation-duration' => $settings['animation-duration'],
                'data-wheel-thickness' => $settings['theme-wheel-thickness'],
                'data-wheel-size' => $settings['theme-wheel-size'],
                'data-is-global' => $settings['is_global'],
                'data-is-botnation' => $settings['is_botnation']
            ]
        );

        $this->add_render_attribute(
            'counter-icon',
            [
                'alt' => 'counter icon',
                'src' => $settings['counter-icon']['url']
            ]
        );

        $this->add_render_attribute(
            'counter-icon-wrapper',
            [
                'class' => ['ssn__counter-icon-wrapper'],
                'data-show-circular' => $settings['show_circular_counter'] == '' ? 'no' : $settings['show_circular_counter'],
                'data-show-icon' => ($settings['show_icon'] == '' || $settings['is_global'] === 'yes') ? 'no' : $settings['show_icon']
            ]
        );

        $icon = sprintf(
            '
            <div %2$s>
                <div class="ssn__circular-counter">
                
                </div>
                <div class="ssn__counter-icon">
                    <img %1$s/>
                </div>
            </div>
            ',
            $this->get_render_attribute_string( 'counter-icon' ),
            $this->get_render_attribute_string( 'counter-icon-wrapper' )
        );

        $text = sprintf(
            '
            <div class="ssn__counter-content">
                <span class="ssn__counter-value">0</span>
                %1$s
                %2$s
            </div>
            ',
            ($settings['show_total_score'] == 'yes' && $settings['is_global'] == '') ? '<span class="ssn__counter-total">0</span>' : '',
            $settings['counter-label'] != '' ? ' ' . $settings['counter-label'] : ''
        );

        $html = sprintf(
            '
            <div %1$s>
                %2$s
                %3$s
            </div>
            ',
            $this->get_render_attribute_string( 'counter-wrapper' ),
            $icon,
            $text
        );

        echo $html;
    }

    protected function _content_template()
    {
        ?>
        <#
        view.addRenderAttribute(
            'counter-wrapper',
            {
                'class': [
                    'ssn__counter',
                    settings['inline_mode'] === 'yes' ? 'ssn__counter-inline' : ''
                ],
                'style': [
                    `--theme-color: ${settings[`theme-color`]};`,
                    `--theme-color-completed: ${settings[`theme-color-completed`]};`,
                    `--theme-label-font-size: ${settings[`theme-label-font-size`]}rem;`,
                    `--theme-label-weight: ${settings[`theme-label-weight`]};`,
                    `--theme-icon-size: ${settings[`theme-icon-size`]}rem;`,
                    `--theme-icon-spacing: ${settings[`theme-icon-spacing`]}em;`,
                    `--theme-wheel-size: ${settings[`theme-wheel-size`]}rem;`,
                    `--theme-wheel-bg-color: ${settings[`theme-wheel-bg-color`]};`,
                    `--theme-wheel-fill-color: ${settings[`theme-wheel-fill-color`]};`
                ].join(' '),
                'data-linked-units': JSON.stringify( settings['sub-modules'] ),
                'data-animation-duration': settings['animation-duration'],
                'data-wheel-thickness': settings['theme-wheel-thickness'],
                'data-wheel-size': settings['theme-wheel-size'],
                'data-is-global': settings['is_global'],
                'data-is-botnation': settings['is_botnation']
            }
        );

        view.addRenderAttribute(
            'counter-icon',
            {
                'alt': 'counter icon',
                'src': settings['counter-icon']['url']
            }
        );

        view.addRenderAttribute(
            'counter-icon-wrapper',
            {
                'class': ['ssn__counter-icon-wrapper'],
                'data-show-circular': (settings['show_circular_counter'] === '' || settings['is_global'] === 'yes') ? 'no' : settings['show_circular_counter'],
                'data-show-icon': settings['show_icon'] === '' ? 'no' : settings['show_icon']
            }
        );

        const icon = `
        <div ${view.getRenderAttributeString('counter-icon-wrapper')}>
            <div class="ssn__circular-counter">

            </div>
            <div class="ssn__counter-icon">
                <img ${view.getRenderAttributeString('counter-icon')}/>
            </div>
        </div>
        `;

        const text = `
        <div class="ssn__counter-content">
            <span class="ssn__counter-value">0</span>${
                (settings['show_total_score'] === 'yes' && settings['is_global'] === '') ? '<span class="ssn__counter-total">0 </span>' : ''
            }${settings['counter-label'] && settings['counter-label'] !== '' ? ` ${settings['counter-label']}` : ''}
        </div>
        `;

        const html = `
        <div ${view.getRenderAttributeString('counter-wrapper')}>
            ${icon}
            ${text}
        </div>
        `;

        print(html);
        #>
        <?php
    }
}
