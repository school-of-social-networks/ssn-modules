<?php

namespace Ssn\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

class SsnAdultCategory extends Widget_Base
{
    public function __construct($data = [], $args = null) {
        parent::__construct($data, $args);

        wp_register_script(
            'ssn-adult-tag-reactive-script',
            plugins_url() . '/ssn-modules/ext-templates/elementor/scripts/ssn-adult-category-reactive-script.js',
            [ 'elementor-frontend' ],
            '1.0.18',
            true
        );
    }

    public function get_name()
    {
        return 'ssn-adult-tag';
    }

    public function get_title()
    {
        return 'SSN Adult Tag';
    }

    public function get_icon()
    {
        return 'fas fa-award';
    }

    public function get_categories()
    {
        return [ 'general' ];
    }

    public function get_style_depends()
    {
        return [ 'ssn-adult-tags-style' ];
    }

    public function get_script_depends() {
        return [ 'ssn-adult-tag-reactive-script' ];
    }

    protected function parse_posts( $posts )
    {
        $output = array();

        if ( $posts )
        {
            foreach ( $posts as $post )
            {
                $post_title = $post->post_title;
                $url = get_permalink( $post->ID );
                $categories = wp_get_post_categories( $post->ID );

                array_push(
                    $output,
                    array(
                        'id' => $post->ID,
                        'title' => $post_title,
                        'url' => $url,
                        'modules' => $categories,
                        'tags' => wp_get_post_tags( $post->ID )
                    )
                );
            }
        }

        return $output;
    }

    protected function get_posts( $tag )
    {
        $settings = $this->get_settings_for_display();
        $posts = get_posts(
            array(
                $settings['tag-source'] => $tag,
                'numberposts' => -1,
                'orderby' => array( 'post_date' => 'ASC' )
            )
        );

        return $this->parse_posts( $posts );
    }

    protected function render_post( $post )
    {
        $this->add_render_attribute(
            'ssn-tag-wrapper' . $post['id'],
            [
                'id' => $post['id'],
                'data-tag-url' => $post['url'],
                'class' => [ 'ssn__tag-element', 'ssn__tag-element-closed' ]
            ]
        );

        return sprintf(
            '
            <div %2$s>
                <div class="ssn__tag-element-head">
                    <div>
                        %1$s
                    </div>
                    <div>
                        <div class="ssn__tag-expand-button">
                            <i arial-hidden="true" class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
                <div class="ssn__tag-content">
                    
                </div>
            </div>
            ',
            $post['title'],
            $this->get_render_attribute_string( 'ssn-tag-wrapper' . $post['id'] )
        );
    }

    protected function render_posts( $posts )
    {
        $html = array();

        foreach ( $posts as $post )
        {
            array_push( $html, $this->render_post( $post ) );
        }

        return join( " ", $html );
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'configuration',
            [
                'label' => __( 'Configuration', 'ssn-adult-tag' )
            ]
        );

        $this->add_control(
            'tag-slug',
            [
                'label' => 'Category/Tag ID',
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'placeholder' => __( 'my-tag', 'ssn-adult-tag' ),
            ]
        );

        $this->add_control(
            'tag-source',
            [
                'label' => __( 'Tag source', 'ssn-adult-tag' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'category',
                'options' => [
                    'category'  => __( 'Category', 'ssn-adult-tag' ),
                    'tag_id'  => __( 'Tag', 'ssn-adult-tag' ),
                ],
            ]
        );

        $this->add_control(
            'theme-active-color',
            [
                'label' => 'Active color',
                'type' => Controls_Manager::COLOR,
                'default' => '#6C63FF'
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $posts = $this->get_posts( $settings['tag-slug'] );
        $this->add_render_attribute(
            'ssn-widget-wrapper',
            [
                'class' => [ 'ssn__tag-list' ],
                'style' => ' --active-color: ' . $settings['theme-active-color'] . ';'
            ]
        );

        echo sprintf(
            '
            <div %2$s>
                %1$s
            </div>
            ',
            $this->render_posts( $posts ),
            $this->get_render_attribute_string( 'ssn-widget-wrapper' )
        );
    }

    protected function _content_template()
    {
        ?>
        <#
        const id = new Date().getTime();

        view.addRenderAttribute(
            'ssn-widget-wrapper',
            {
                'data-elementor-id': id,
                'class': [ 'ssn__tag-list' ],
                'style': `--active-color: ${settings['theme-active-color']};`
            }
        );

        const SSN_fetcher = async (url, params) => {
            const res = await fetch(
                url,
                params
            );

            const tres = await res.text();

            try {
                const jres = JSON.parse(tres);
                return jres;
            } catch (e) {
                console.error(e);
                console.error(tres);

                return null;
            }
        };

        const renderElements = async () => {
            const posts = await SSN_fetcher(
                "<?php echo content_url( '/plugins/ssn-modules/standalone/fetch_posts.php' )?>",
                {
                    method: 'POST',
                    body: JSON.stringify({[settings['tag-source']]: settings['tag-slug']})
                }
            );

            const html = posts.map(
                post => `
                    <div data-tag-url="${post.url}" id="${post.id}" class="ssn__tag-element ssn__tag-element-closed">
                        <div class="ssn__tag-element-head">
                            <div>
                                ${post.title}
                            </div>
                            <div>
                                <div class="ssn__tag-expand-button">
                                    <i arial-hidden="true" class="fas fa-chevron-right"></i>
                                </div>
                            </div>
                        </div>
                        <div class="ssn__tag-content">

                        </div>
                    </div>
                `
            ).join(' ');

            document.querySelector('#elementor-preview-iframe').contentWindow.document.
            querySelector(`[data-elementor-id="${id}"]`).innerHTML = html;
        };

        print (
        `
            <div ${view.getRenderAttributeString('ssn-widget-wrapper')}>

            </div>
        `
        );

        renderElements();
        #>
        <?php
    }
}
