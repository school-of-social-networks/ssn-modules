<?php

namespace Ssn\Widgets;

use Elementor\Controls_Manager;
use Elementor\Utils;
use Ssn\Base\SsnSubModuleBase;

class SsnSubModuleWidget extends SsnSubModuleBase
{
    public function __construct($data = [], $args = null)
    {
        parent::__construct($data, $args);

        wp_register_script(
            'ssn-reactive-script',
            plugins_url() . '/ssn-modules/ext-templates/elementor/scripts/ssn-sub-module-reactive-script.js',
            [ 'elementor-frontend' ],
            '1.3.5',
            true
        );
    }

    public function get_name()
    {
        return 'ssn-sub-module';
    }

    public function get_title()
    {
        return __( 'SSN Sub-Module', 'ssn' );
    }

    public function get_icon()
    {
        return 'fas fa-award';
    }

    public function get_categories()
    {
        return [ 'general' ];
    }

    public function get_style_depends()
    {
        return [
            'ssn-unit',
            'ssn-unit-header',
            'ssn-unit-cards',
            'ssn-unit-elements',
            'ssn-unit-content',
            'ssns-cards'
        ];
    }

    public function get_script_depends() {
        return [ 'ssn-reactive-script' ];
    }

    protected function get_posts( $category )
    {
        $posts = get_posts(
            array( 'category' => $category, 'numberposts' => -1 )
        );

        return $this->parse_posts( $posts );
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'configuration',
            [
                'label' => __( 'Configuration', 'ssn' )
            ]
        );

        $this->add_control(
            'module-slug',
            [
                'label' => 'Sub-module category',
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'placeholder' => __( 'ID for your category', 'ssn' ),
            ]
        );

        $this->add_control(
            'section-title',
            [
                'label' => 'Section title',
                'type' => Controls_Manager::TEXTAREA,
                'placeholder' => __( 'Enter a title for your section', 'ssn' ),
                'dynamic' => [
                    'active' => true,
                ]
            ]
        );

        $this->add_control(
            'card-image',
            [
                'label' => __( 'Back of cards image', 'elementor-pro' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'theme',
            [
                'label' => __( 'Theme', 'ssn' )
            ]
        );

        $this->add_control(
            'color_scheme',
            [
                'label' => __( 'Color Scheme', 'ssn' ),
                'type' => \Elementor\Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'theme-txt-color',
            [
                'label' => 'Text Color',
                'type' => Controls_Manager::COLOR,
                'default' => '#252525'
            ]
        );

        $this->add_control(
            'theme-txt-complete-color',
            [
                'label' => 'Text Color (completed)',
                'type' => Controls_Manager::COLOR,
                'default' => '#6C63FF'
            ]
        );

        $this->add_control(
            'theme-card-color',
            [
                'label' => 'Cards color',
                'type' => Controls_Manager::COLOR,
                'default' => '#6C63FF'
            ]
        );

        $this->add_control(
            'theme-bg-color',
            [
                'label' => 'Background color',
                'type' => Controls_Manager::COLOR,
                'default' => '#FFFFFF'
            ]
        );

        $this->end_controls_section();
    }

    protected function render_header( $posts )
    {
        $settings = $this->get_settings_for_display();
        $cards_count = count( $posts );

        $this->add_inline_editing_attributes( 'section-title', 'basic' );
        $html_title = sprintf(
            '<div class="ssn__unit-header-title"><div>%1$s</div></div>',
            $settings['section-title']
        );

        $html_cards_icon = sprintf( '
            <div class="ssn__unit-header-block ssn__unit-icon">
                <svg 
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 467.06 505.99"
                >
                    <g fill="currentColor">
                        <path d="M465.06,83.27A44.92,44.92,0,0,0,451.87,52a43.69,43.69,0,0,0-31.12-12.88h-.4l-81.21.39-2.43-10.34A37.64,37.64,0,0,0,291.3,1L29.2,62.34A37.83,37.83,0,0,0,1,107.85l86.4,368.9A37.83,37.83,0,0,0,124.16,506a34.29,34.29,0,0,0,8.65-1.05L175.29,495l247.58-1.3a44.59,44.59,0,0,0,44.19-44.61ZM395,443.54A37.83,37.83,0,0,0,423.2,398L343,55.58l77.48-.39h.2a28.47,28.47,0,0,1,28.4,28.21l2,365.79a28.56,28.56,0,0,1-28.31,28.5l-177.49.91Zm-3.61-15.6L173,479.15l-.6.14-.67.15-42.6,10a21.83,21.83,0,0,1-26.2-16.31l-86.4-369A21.84,21.84,0,0,1,32.81,77.94L295,16.54a23.16,23.16,0,0,1,5.07-.55,21.9,21.9,0,0,1,21.25,16.86l86.4,368.9A21.84,21.84,0,0,1,391.4,427.94Z">
                    
                        </path>
                        <path d="M303.23,251.73l-46.82-32.66-4.88-56.67a8,8,0,0,0-12.78-5.7l-45.4,34.14L137.8,177.71a8,8,0,0,0-9.4,10.4L147.07,242l-29.45,48.58a8,8,0,0,0,7,12.15l57.06-.94L219,344.92a8,8,0,0,0,6.05,2.77,8.5,8.5,0,0,0,1.66-.17,8,8,0,0,0,6-5.51l16.51-54.39,52.52-21.95a8,8,0,0,0,1.49-13.94Zm-63.66,22.58a8,8,0,0,0-4.57,5.06l-13.39,44.11-30.3-35a8,8,0,0,0-6.05-2.77h-.13l-46.33.76L162.7,247a8,8,0,0,0,.72-6.77l-15.14-43.64,45,10.65a8,8,0,0,0,6.65-1.4l36.85-27.72,4,46a8,8,0,0,0,3.39,5.87l38,26.47Z">
                        
                        </path>
                    </g> 
                </svg>
            </div>
        ' );

        $html_cards_count = sprintf(
            '<div class="ssn__unit-header-block ssn__unit-header-progress">
                <div><span class="ssn__unit-currently-unlocked-cards">0</span>/%1$s</div>
            </div>',
            $cards_count
        );

        $html_expand_button = "
            <div class=\"ssn__unit-header-double-block ssn__unit-expand-button-wrapper\">
                <div class=\"ssn__unit-expand-button\">
                    <i arial-hidden=\"true\" class=\"fas fa-chevron-circle-right\"></i>
                </div>
            </div>
        ";

        return sprintf(
            '<div class="ssn__unit-header">
                <div class="ssn__unit-cards-count-wrapper">%1$s %2$s</div> %3$s %4$s
            </div>',
            $html_cards_icon,
            $html_cards_count,
            $html_title,
            $html_expand_button
        );
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $posts = $this->get_posts($settings['module-slug']);
        $this->add_render_attribute(
            'ssn-widget-wrapper',
            [
                'class' => [ 'ssn__unit-wrapper' ],
                'id' => $settings['module-slug'],
                'style' => '--background-color: ' . $settings['theme-bg-color'] . ';' .
                    ' --text-color: ' . $settings['theme-txt-color'] . ';' .
                    ' --card-color: ' . $settings['theme-card-color'] . ';' .
                    ' --fill-color: ' . $settings['theme-txt-complete-color'] . ';',
                'data-sub-module-id' => $settings['module-slug']
            ]
        );

        $html = sprintf(
            '
            <div %3$s>
                %1$s
                <div class="ssn__unit-content ssn__unit-content-hidden" style="height: 0;">
                    %2$s
                </div>
            </div>
            ',
            $this->render_header( $posts ),
            $this->render_unit_content( $posts ),
            $this->get_render_attribute_string( 'ssn-widget-wrapper' )
        );

        echo $html;
    }

    protected function _content_template()
    {
        ?>
        <#
        const cardsCount = settings['cards'].length;

        view.addInlineEditingAttributes( 'section-title', 'basic' );
        const htmlTitle = `
        <div class="ssn__unit-header-title">
            <div>${settings['section-title']}</div>
        </div>
        `;

        const htmlCardsIcon = `
        <div class="ssn__unit-header-block ssn__unit-icon">
            <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 467.06 505.99"
            >
                <g fill="currentColor">
                    <path d="M465.06,83.27A44.92,44.92,0,0,0,451.87,52a43.69,43.69,0,0,0-31.12-12.88h-.4l-81.21.39-2.43-10.34A37.64,37.64,0,0,0,291.3,1L29.2,62.34A37.83,37.83,0,0,0,1,107.85l86.4,368.9A37.83,37.83,0,0,0,124.16,506a34.29,34.29,0,0,0,8.65-1.05L175.29,495l247.58-1.3a44.59,44.59,0,0,0,44.19-44.61ZM395,443.54A37.83,37.83,0,0,0,423.2,398L343,55.58l77.48-.39h.2a28.47,28.47,0,0,1,28.4,28.21l2,365.79a28.56,28.56,0,0,1-28.31,28.5l-177.49.91Zm-3.61-15.6L173,479.15l-.6.14-.67.15-42.6,10a21.83,21.83,0,0,1-26.2-16.31l-86.4-369A21.84,21.84,0,0,1,32.81,77.94L295,16.54a23.16,23.16,0,0,1,5.07-.55,21.9,21.9,0,0,1,21.25,16.86l86.4,368.9A21.84,21.84,0,0,1,391.4,427.94Z">

                    </path>
                    <path d="M303.23,251.73l-46.82-32.66-4.88-56.67a8,8,0,0,0-12.78-5.7l-45.4,34.14L137.8,177.71a8,8,0,0,0-9.4,10.4L147.07,242l-29.45,48.58a8,8,0,0,0,7,12.15l57.06-.94L219,344.92a8,8,0,0,0,6.05,2.77,8.5,8.5,0,0,0,1.66-.17,8,8,0,0,0,6-5.51l16.51-54.39,52.52-21.95a8,8,0,0,0,1.49-13.94Zm-63.66,22.58a8,8,0,0,0-4.57,5.06l-13.39,44.11-30.3-35a8,8,0,0,0-6.05-2.77h-.13l-46.33.76L162.7,247a8,8,0,0,0,.72-6.77l-15.14-43.64,45,10.65a8,8,0,0,0,6.65-1.4l36.85-27.72,4,46a8,8,0,0,0,3.39,5.87l38,26.47Z">

                    </path>
                </g>
            </svg>
        </div>
        `;

        const htmlCardsCount = `
        <div class="ssn__unit-header-block ssn__unit-header-progress">
            <div><span class="ssn__unit-currently-unlocked-cards">0</span>/${cardsCount}</div>
        </div>
        `;

        const htmlExpandButton = `
        <div class=\"ssn__unit-header-double-block ssn__unit-expand-button-wrapper\">
            <div class=\"ssn__unit-expand-button\">
                <i arial-hidden=\"true\" class=\"fas fa-chevron-circle-right\"></i>
            </div>
        </div>
        `;

        const htmlHeader = `
        <div class="ssn__unit-header">
            <div class="ssn__unit-cards-count-wrapper">
                ${htmlCardsIcon}
                ${htmlCardsCount}
            </div>
            ${htmlTitle}
            ${htmlExpandButton}
        </div>
        `;

        const cards = [];
        const {url} = settings['card-image'] || {};

        const htmlCards = `
        <div class="ssn__unit-card-preview" style="--ssn-card-animation-delay: 0s; pointer-events: none!important;">
            <img src="<?php echo content_url() . '/plugins/ssn-modules/assets/module1_1b.png' ?>" alt="card illustration" class="ssn__card-illustration"/>
            <img src="${url || ''}" alt="card placeholder" class="ssn__card-placeholder"/>
        </div>
        <div class="ssn__unit-card-preview" style="--ssn-card-animation-delay: 1s; pointer-events: none!important;">
            <img src="<?php echo content_url() . '/plugins/ssn-modules/assets/module1_1b.png' ?>" alt="card illustration" class="ssn__card-illustration"/>
            <img src="${url || ''}" alt="card placeholder" class="ssn__card-placeholder"/>
        </div>
        <div class="ssn__unit-card-preview" style="--ssn-card-animation-delay: 2s; pointer-events: none!important;">
            <img src="<?php echo content_url() . '/plugins/ssn-modules/assets/module1_1b.png' ?>" alt="card illustration" class="ssn__card-illustration"/>
            <img src="${url || ''}" alt="card placeholder" class="ssn__card-placeholder"/>
        </div>
        `;

        view.addRenderAttribute(
            'ssn-widget-wrapper',
            {
                'class': [ 'ssn__unit-wrapper' ],
                'style': [
                    `--background-color: ${settings['theme-bg-color']};`,
                    `--text-color: ${settings['theme-txt-color']};`,
                    `--card-color: ${settings['theme-card-color']};`,
                    `--fill-color: ${settings['theme-txt-complete-color']};`
                ].join(' '),
                'id': settings['module-slug'] || '',
                'data-sub-module-id': settings['module-slug'] || ''
            }
        );

        print(
            `
            <div ${view.getRenderAttributeString('ssn-widget-wrapper')}>
                ${htmlHeader}
                <div class="ssn__unit-content ssn__unit-content-hidden" style="height: 0;">
                    <div class="ssn__unit-elements">

                    </div>
                    <div class="ssn__unit-cards-preview">
                        ${htmlCards}
                    </div>
                </div>
            </div>
            `
        );
        #>
        <?php
    }
}
