<?php

namespace Ssn\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;

class SsnProgressBar extends Widget_Base
{
    public function __construct($data = [], $args = null) {
        parent::__construct($data, $args);

        wp_register_script(
            'ssn-progress-bar-reactive-script',
            plugins_url() . '/ssn-modules/ext-templates/elementor/scripts/ssn-progress-bar-reactive-script.js',
            [ 'elementor-frontend' ],
            '1.0.27',
            true
        );
    }

    public function get_name()
    {
        return 'ssn-progress-bar';
    }

    public function get_title()
    {
        return 'SSN Progress Bar';
    }

    public function get_icon()
    {
        return 'fas fa-award';
    }

    public function get_categories()
    {
        return [ 'general' ];
    }

    public function get_style_depends()
    {
        return [ 'ssn-progress-bar-style' ];
    }

    public function get_script_depends() {
        return [ 'ssn-progress-bar-reactive-script' ];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'configuration',
            [
                'label' => __( 'Configuration', 'ssn-counter' )
            ]
        );

        $this->add_control(
            'bar-icon',
            [
                'label' => __( 'Bar icon', 'elementor-pro' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ]
            ]
        );

        $this->add_control(
            'theme-bg-color',
            [
                'label' => 'Background Color',
                'type' => Controls_Manager::COLOR,
                'default' => '#ffffff'
            ]
        );

        $this->add_control(
            'theme-fill-color',
            [
                'label' => 'Fill Color',
                'type' => Controls_Manager::COLOR,
                'default' => '#6C63FF'
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_render_attribute(
            'bar-wrapper',
            [
                'class' => [ 'ssn__progress-bar' ],
                'data-startoff' => '0',
                'style' => '--bg-color: ' . $settings['theme-bg-color'] . ';' .
                    '--bg-fill-color: ' . $settings['theme-fill-color'] . ';'
            ]
        );

        $this->add_render_attribute(
            'bar-icon',
            [
                'class' => [ 'ssn__progress-bar-icon' ],
                'alt' => 'progress bar icon',
                'src' => $settings['bar-icon']['url'],
                'style' => $settings['bar-icon']['url'] == Utils::get_placeholder_image_src() ?
                    'display: none!important;' : '',
            ]
        );

        echo sprintf(
            '
            <div class="ssn__progress-bar-wrapper">
                <div %1$s>
                    <div class="ssn__progress-bar-display">
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                    </div>
                    <img %2$s/>
                </div>
            </div>
            ',
            $this->get_render_attribute_string( 'bar-wrapper' ),
            $this->get_render_attribute_string( 'bar-icon' )
        );
    }

    protected function _content_template()
    {
        ?>
        <#
        view.addRenderAttribute(
            'bar-wrapper',
            {
                'class': [ 'ssn__progress-bar' ],
                'data-startoff': '0',
                'style': `--bg-color: ${settings['theme-bg-color']}; --bg-fill-color: ${settings['theme-fill-color']};`
            }
        );

        view.addRenderAttribute(
            'bar-icon',
            {
                'class': [ 'ssn__progress-bar-icon' ],
                'alt': 'progress bar icon',
                'src': settings['bar-icon']['url'],
                'style': settings['bar-icon']['url'] === "<?php echo Utils::get_placeholder_image_src()?>" ?
                    'display: none!important;' : ''
            }
        );

        print(
            `
            <div class="ssn__progress-bar-wrapper">
                <div ${view.getRenderAttributeString('bar-wrapper')}>
                    <div class="ssn__progress-bar-display">
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                        <div class="ssn__progress-bar-display-frac"><div></div></div>
                    </div>
                    <img ${view.getRenderAttributeString('bar-icon')}/>
                </div>
            </div>
            `
        );
        #>
        <?php
    }
}