<?php

namespace Ssn\Base;
use Elementor\Widget_Base;

class SsnSubModuleBase extends Widget_Base
{
    public function __construct($data = [], $args = null) {
        parent::__construct($data, $args);
    }

    protected function parse_posts( $posts )
    {
        $output = array();

        if ( $posts )
        {
            foreach ( $posts as $post )
            {
                $post_title = $post->post_title;
                $thumbail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0];
                $url = get_permalink( $post->ID );
                $categories = wp_get_post_categories( $post->ID );

                array_push(
                    $output,
                    array(
                        'id' => $post->ID,
                        'title' => $post_title,
                        'url' => $url,
                        'thumbail' => $thumbail,
                        'modules' => $categories,
                        'tags' => wp_get_post_tags( $post->ID )
                    )
                );
            }
        }

        return $output;
    }

    protected function render_card( $index, $post, $placeholder )
    {
        $baseParameters = [
            'class' => ['ssn__unit-card-preview'],
            'data-card-id' => $post['id'],
            'data-card-modules' => json_encode($post['modules']),
            'data-card-tags' => json_encode($post['tags']),
            'data-card-css' => $post['css'],
            'style' => '--ssn-card-animation-delay: ' . $index . 's;'
        ];

        $this->add_render_attribute(
            $index . 'card-attributes',
            $baseParameters
        );

        return sprintf(
            '<div %1$s>
                <img src="%2$s" alt="card illustration" class="ssn__card-illustration"/>
                <img src="%3$s" alt="card placeholder" class="ssn__card-placeholder"/>
                <div class="ssn__card-preview-title"><div>%4$s</div></div>
            </div>',
            $this->get_render_attribute_string( $index . 'card-attributes' ),
            $post['thumbail'],
            $placeholder,
            $post['title']
        );
    }

    protected function render_unit( $index, $post )
    {
        $this->add_render_attribute(
            $index . 'unit-attributes',
            [
                'class' => ['ssn__unit-element-hidden', 'ssn__unit-element'],
                'style' => 'height: 0;',
                'data-unit-id' => $post['id'],
                'data-unit-url' => $post['url']
            ]
        );

        return sprintf(
            '<div %2$s>
                <div class="ssn__unit-card-view ssn__unit-card-preview-unlocked">
                    <img src="%1$s" alt="card illustration" class="ssn__card-illustration"/>
                    <div class="ssn__card-preview-title"><div>%3$s</div></div>
                </div>
                <div class="ssn__unit-data">
                    <div class="ssn__unit-lessons">
                        <div class="ssn__unit-loader">
                            <svg
                              width="200px"
                              height="200px"
                              xmlns="http://www.w3.org/2000/svg"
                              viewBox="0 0 100 100"
                              preserveAspectRatio="xMidYMid"
                              class="lds-eclipse"
                              style="background: none;"
                            >
                                <path
                                  ng-attr-d="{{config.pathCmd}}"
                                  ng-attr-fill="{{config.color}}"
                                  stroke="none"
                                  d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50"
                                  fill="currentColor"
                                  transform="rotate(141.169 50 51)"
                                >
                                    <animateTransform
                                      attributeName="transform"
                                      type="rotate"
                                      calcMode="linear"
                                      values="0 50 51;360 50 51"
                                      keyTimes="0;1"
                                      dur="1s"
                                      begin="0s"
                                      repeatCount="indefinite"
                                    >
                            
                                    </animateTransform>
                                </path>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>',
            $post['thumbail'],
            $this->get_render_attribute_string( $index . 'unit-attributes' ),
            $post['title']
        );
    }

    protected function render_unit_content( $posts )
    {
        $settings = $this->get_settings_for_display();
        $cards = array();
        $units = array();

        $placeholder = '';

        if ( ! empty( $settings['card-image']['url'] ) ) {
            $placeholder = $settings['card-image']['url'];
        }

        foreach ( $posts as $index => $post )
        {
            array_push( $cards, $this->render_card( $index, $post, $placeholder ) );
            array_push( $units, $this->render_unit( $index, $post ) );
        }

        return sprintf(
            '
            <div class="ssn__unit-elements">
                %1$s
            </div>
            <div class="ssn__unit-cards-preview">
                %2$s
            </div>
            ',
            join('', $units),
            join('', $cards)
        );
    }

    public function get_name()
    {
        // TODO: Implement get_name() method.
    }
}
