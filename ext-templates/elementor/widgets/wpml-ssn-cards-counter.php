<?php

class WpmlSsnCounter extends WPML_Elementor_Module_With_Items
{
		public function get_items_field()
		{
				return 'sub-modules';
		}

		public function get_fields()
		{
				return array( 'sub_module_id' );
		}

		protected function get_title( $field )
		{
				switch( $field )
				{
						case 'sub_module_id':
								return esc_html__( 'Counter: module id', 'wpml-string-translation' );

						default:
								return '';
				}
		}

		protected function get_editor_type( $field )
		{
				switch( $field )
				{
						case 'sub_module_id':
								return 'LINE';

						default:
								return '';
				}
		}
}