// Return timestamp and positions for an animation. Timer is a function that takes time as parameter to
// return position : f(t) = p. Time is on the x axis.
const getAnimationSteps = (timer, bounds) => {
    // Time boundaries allow to use a particular section of a curve for the timing animation.
    // For example, a sinus curve starts at 0, but the peak (the endpoint) is at PI/2.
    // The time interval [0, d] will then be scaled to [0, PI/2].
    const tst = bounds.timeStart || 0;
    const ted = bounds.timeEnd || 1000; // Avoid losses with large animation time.

    if (ted < tst) {
        throw new Error(`End time boundary ${tst} cannot be lower than Start time boundary ${ted}`);
    }

    // Extract bounds for quicker computation.
    const lx = bounds.posStart;
    const hx = bounds.posEnd;

    if (hx < lx) {
        throw new Error(`End position boundary ${hx} cannot be lower than Start position boundary ${lx}`);
    }

    const s = bounds.timeInterval;
    const d = bounds.duration;
    const ms = bounds.maxSpeed;

    // Calculate the actual positions range on the y axis.
    const ln = timer(tst);
    const hn = timer(ted);

    // Scale factors.
    const f = (hx - lx) / (hn - ln); // Position scale factor.
    const tf = (ted - tst) / d; // Time scale factor.

    // Output array contains {timestamp, position}.
    const o = [];

    // Keep track of last position to avoid adding multiple tuples with same position.
    let last = 0;
    let offset = 0;

    // Loop through each time steps (framerate).
    for (let i = 0; i <= d + s - 1; i += s) {
        // Calculate position for timestamp (current frame).
        // This is the unscaled absolute position, meaning it is not yet relative to our position boundaries.
        const up = timer( tst + (tf * i) );

        // Scale the position. Since both absolute and relative position may have a start offset, we first need
        // to translate the absolute position to start at 0 [ln, hn] -> [0, hn - ln], then again, once scaled,
        // add the relative position offset back.
        let p = Math.floor( ((up - ln) * f) ) + lx;

        if (p > last) {
            if (p > hx) {
                p = hx;
                // We reached last position, no need to continue further.
                break;
            }

            if (ms != null) {
                let hold = last + ms;
                while (hold < p) {
                    o.push({timestamp: i + offset, position: hold});
                    offset += s;
                    hold += ms;
                }
            }

            last = p;
            o.push({timestamp: i + offset, position: p});
        }
    }

    return o;
};

const animator = (timer, callback, bounds) => {
    const steps = getAnimationSteps(timer, bounds);

    for (const step of steps) {
        setTimeout(() => callback(step.position, step.timestamp), step.timestamp);
    }
};

const displayNumber = (num, sep = ' ') => {
    let snum = num.constructor.name === 'String' ? num : `${num}`;

    let o = '';
    for (let i = 0; i < snum.length / 3; i++) {
        if (i === 0) {
            o = `${snum.slice(-3)}`;
        } else {
            o = `${snum.slice(-3 * (i+1), -3 * i)}${sep}${o}`
        }
    }

    return o;
};

const displayToNum = val => parseInt(
    val.split('').filter(x => '0123456789'.includes(x)).join(''),
    10
);

const easeOut = x => 1000 - ( Math.pow(1000 - x, 3) );

class SsnCounterHandler extends elementorModules.frontend.handlers.Base {
    getDefaultSettings() {
        return {
            selectors: {
                total: '.ssn__counter-total',
                value: '.ssn__counter-value',
                wrapper: '.ssn__counter',
                wheel: '.ssn__circular-counter'
            }
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings( 'selectors' );
        return {
            $total: this.$element.find( selectors.total ),
            $wrapper: this.$element.find( selectors.wrapper ),
            $value: this.$element.find( selectors.value ),
            $wheel: this.$element.find( selectors.wheel )
        };
    }

    getLinkedUnits() {
        return (JSON.parse(this.elements.$wrapper[0].dataset.linkedUnits) || []).map(
            ({sub_module_id} = {}) => sub_module_id
        );
    }

    async animate( el, start, end, duration ) {
        if (end > start + 1) {
            const over = end - start - 5;

            animator(
                easeOut,
                p => el.innerHTML = displayNumber(Math.trunc(p)),
                {
                    posStart: start,
                    posEnd: end,
                    duration: duration + (over > 0 ? 3 * Math.log(over) : 0),
                    timeInterval: 10,
                    maxSpeed: 10000
                }
            );
        } else {
            el.innerHTML = displayNumber(end);
        }
    }

    updateDynamicCount( val ) {
        for (const el of [...this.elements.$value]) {
            this.animate(
                el,
                displayToNum(el.innerHTML),
                val,
                parseInt(this.elements.$wrapper[0].dataset.animationDuration, 10)
            ).catch(console.error);
        }
    }

    updateCounter() {
        const counter = SSN_getAndUpdateProgress({
            modules: this.getLinkedUnits.bind(this)()
        });
        const overallCount = counter.overall.length;
        const progressCount = counter.progress.length;

        for (const el of [...this.elements.$total]) {
            el.innerHTML = overallCount;
        }

        this.updateDynamicCount.bind(this)(progressCount);

        const size = parseFloat( this.elements.$wrapper[0].dataset.wheelSize );
        const thickness = parseFloat( this.elements.$wrapper[0].dataset.wheelThickness );
        const relativeThickness = ( thickness / size ) * 1000;

        for (const el of [...this.elements.$wheel]) {
            el.innerHTML = generateSvg({
                width: 1000,
                height: 1000,
                struct: [
                    [
                        'g',
                        {
                            children: generateArcs(progressCount, overallCount, {
                                color: 'var(--theme-wheel-fill-color)',
                                backgroundColor: 'var(--theme-wheel-bg-color)',
                                'stroke-width': relativeThickness,
                                fill: 'none',
                                x: 500,
                                y: 500,
                                r: roundTo( 500 - (relativeThickness / 2) )
                            })
                        }
                    ]
                ]
            });
        }

        if (overallCount <= progressCount) {
            this.elements.$wrapper[0].classList.add('ssn__counter-complete');
        } else {
            this.elements.$wrapper[0].classList.remove('ssn__counter-complete');
        }
    }

    bindEvents() {
        this.updateCounter.bind(this)();

        let units = this.getLinkedUnits.bind(this)();
        if (this.elements.$wrapper[0].dataset.isGlobal === 'yes') {
            if (units.length === 1 && units[0] === "*") units = [];

            setInterval(async () => {
                this.updateDynamicCount.bind(this)(await SSN_getCount(...units));
            }, 1000);
        } else if (this.elements.$wrapper[0].dataset.isBotnation === 'yes') {
            SSN_getChatbotCount().then(this.updateDynamicCount.bind(this));
        } else {
            for (const unit of units) {
                document.addEventListener(
                    `update-unit-${unit}`,
                    this.updateCounter.bind(this)
                );
            }
        }
    }
}

jQuery( window ).on(
    'elementor/frontend/init',
    () => {
        const addHandler = ( $element ) => {
            elementorFrontend.elementsHandler.addHandler( SsnCounterHandler, { $element } );
        };

        elementorFrontend.hooks.addAction( 'frontend/element_ready/ssn-counter.default', addHandler );
    }
);