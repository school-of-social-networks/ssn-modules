class SsnAdultCategoryHandler extends elementorModules.frontend.handlers.Base {
    expandTimer = () => {};

    getDefaultSettings() {
        return {
            selectors: {
                expander: '.ssn__tag-element-head',
                expanderIcon: '.ssn__tag-expand-button',
                sectionContainer: '.ssn__tag-element',
                content: '.ssn__tag-content'
            }
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings( 'selectors' );
        return {
            $expander: this.$element.find( selectors.expander ),
            $expanderIcon: this.$element.find( selectors.expanderIcon ),
            $sectionContainer: this.$element.find( selectors.sectionContainer ),
            $content: this.$element.find( selectors.content )
        };
    }

    bindEvents() {
        this.elements.$expander.on( 'click', this.expandCards.bind(this) );

        for (const unit of this.elements.$sectionContainer) {
            fetch(
                unit.dataset.tagUrl,
                {method: "GET"}
            )
                .then(
                    async result => {
                        unit.querySelector('.ssn__tag-content').innerHTML = await result.text();
                    }
                )
                .catch(console.error);
        }
    }

    async expandCards(e) {
        clearTimeout(this.expandTimer);

        let target = e.target;
        while (!target.classList.contains('ssn__tag-element') && target !== document.body) {
            target = target.parentElement;
        }

        const isExpanded = rotateExpandButton( target.querySelector('.ssn__tag-expand-button') );
        const content = target.querySelector('.ssn__tag-content');

        if (isExpanded) {
            content.classList.remove('ssn__tag-content-hidden');
            target.classList.add('ssn__tag-element-expanded');
            content.style.height = 'unset';
        } else {
            content.classList.add('ssn__tag-content-hidden');
            target.classList.remove('ssn__tag-element-expanded');
            content.style.height = '0';
        }
    }
}

jQuery( window ).on(
    'elementor/frontend/init',
    () => {
        const addHandler = ( $element ) => {
            elementorFrontend.elementsHandler.addHandler( SsnAdultCategoryHandler, { $element } );
        };

        elementorFrontend.hooks.addAction( 'frontend/element_ready/ssn-adult-tag.default', addHandler );
    }
);