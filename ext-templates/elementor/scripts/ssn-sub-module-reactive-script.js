class SsnSubModuleHandler extends elementorModules.frontend.handlers.Base {
    expandTimer = () => {};

    getDefaultSettings() {
        return {
            selectors: {
                expander: '.ssn__unit-header',
                expanderIcon: '.ssn__unit-expand-button',
                progress: '.ssn__unit-currently-unlocked-cards',
                sectionContainer: '.ssn__unit-wrapper'
            }
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings( 'selectors' );
        return {
            $expander: this.$element.find( selectors.expander ),
            $expanderIcon: this.$element.find( selectors.expanderIcon ),
            $progress: this.$element.find( selectors.progress ),
            $sectionContainer: this.$element.find( selectors.sectionContainer ),
            ...SSN_Module_getDefaultElements(this)
        };
    }

    updateProgress(e) {
        const moduleId = this.elements.$sectionContainer[0].dataset.subModuleId;
        const progress = SSN_Module_updateProgress(this, {module: moduleId})(e);

        // Update counter numeric value displayed in unit header.
        this.elements.$progress.html(progress.length);

        // Update status if all cards are unlocked or not.
        if (progress.length >= this.elements.$cards.length) {
            this.$element.addClass('ssn__unit-complete');
        } else {
            this.$element.removeClass('ssn__unit-complete');
        }
    }

    updateRows = () => SSN_Module_updateRows(this)

    bindEvents() {
        this.updateProgress.bind(this)({});

        document.addEventListener(
            `update-unit-${this.elements.$sectionContainer[0].dataset.subModuleId}`,
            this.updateProgress.bind(this)
        );

        this.elements.$expander.on( 'click', this.expandCards.bind(this) );
        SSN_Module_bindEvents(this);
        SSN_Module_loadContent(this);

        const params = new URLSearchParams(window.location.search);
        const cardID = params.get('cardID');

        if (cardID != null) {
            const card = [...this.elements.$cards].find(x => x.dataset.cardId === cardID);
            if (card != null) {
                this.expandCards(true).then(
                    () => card.click()
                );
            }
        }
    }

    async expandCards(openOnly = false) {
        clearTimeout(this.expandTimer);
        const isExpanded = rotateExpandButton( this.elements.$expanderIcon[0] );

        if (isExpanded) {
            this.elements.$content.removeClass('ssn__unit-content-hidden');
            this.elements.$content.css('height', 'unset');
            for (const c of [...this.elements.$cards]) {
                await new Promise(resolve => setTimeout(resolve, 300));
                c.classList.add('ssn__unit-card-post-expanded');
            }
        } else if (openOnly !== true) {
            this.elements.$content.addClass('ssn__unit-content-hidden');
            for (const c of [...this.elements.$cards]) {
                c.classList.remove('ssn__unit-card-post-expanded');
            }

            this.elements.$content.css('height', '0');
        }
    }
}

jQuery( window ).on(
    'elementor/frontend/init',
    () => {
        const addHandler = ( $element ) => {
            elementorFrontend.elementsHandler.addHandler( SsnSubModuleHandler, { $element } );
        };

        elementorFrontend.hooks.addAction( 'frontend/element_ready/ssn-sub-module.default', addHandler );
    }
);