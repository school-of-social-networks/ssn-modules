class SsnChildCategoryHandler extends elementorModules.frontend.handlers.Base {
    getDefaultSettings() {
        return {
            selectors: {
                sectionContainer: '.ssn__unit-content'
            }
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings( 'selectors' );
        return {
            $sectionContainer: this.$element.find( selectors.sectionContainer ),
            ...SSN_Module_getDefaultElements(this)
        };
    }

    updateProgress(e) {
        const moduleId = this.elements.$sectionContainer[0].dataset.subModuleId;
        SSN_Module_updateProgress(this, {tags: [moduleId]})(e);
    }

    updateRows = () => SSN_Module_updateRows(this)

    bindEvents() {
        this.updateProgress.bind(this)({});
        SSN_Module_loadContent(this);
        SSN_Module_bindEvents(this);
    }
}

jQuery( window ).on(
    'elementor/frontend/init',
    () => {
        const addHandler = ( $element ) => {
            elementorFrontend.elementsHandler.addHandler( SsnChildCategoryHandler, { $element } );
        };

        elementorFrontend.hooks.addAction( 'frontend/element_ready/ssn-child-tag.default', addHandler );
    }
);