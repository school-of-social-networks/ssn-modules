class SsnProgressBar extends elementorModules.frontend.handlers.Base {
    getDefaultSettings() {
        return {
            selectors: {
                wrapper: '.ssn__progress-bar',
                bar: '.ssn__progress-bar-display',
                fracs: '.ssn__progress-bar-display-frac',
                icon: '.ssn__progress-bar-icon'
            }
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings( 'selectors' );
        return {
            $wrapper: this.$element.find( selectors.wrapper ),
            $bar: this.$element.find( selectors.bar ),
            $fracs: this.$element.find( selectors.fracs ),
            $icon: this.$element.find( selectors.icon )
        };
    }

    displayProgressBar(p, unit, t, o = 0) {
        if (o < 0) o = 0;
        unit.querySelector('div').style.transition = `ease-out ${0.6 / (t + 1)}s ${(o * (0.6 / (t + 1)))}s`;
        unit.querySelector('div').style.width = `${p}%`;
    }

    updateProgress() {
        const units = document.querySelectorAll('.ssn__unit-wrapper');
        const fracs = this.elements.$fracs;
        const startoffset = parseFloat(this.elements.$wrapper[0].dataset.startoff);

        const counter = SSN_getAndUpdateProgress({
            modules: [...units].map(({id}) => id)
        });
        const overallCount = counter.overall.length;
        const progressCount = counter.progress.length;

        const percent = (progressCount / overallCount) * 100;
        const timer = Math.floor(Math.abs(percent - startoffset) / 25);

        const currentStart = Math.floor(percent / 25);
        const lastStart = Math.floor(startoffset / 25);

        const reverse = currentStart < lastStart;

        switch (currentStart) {
            case 0:
                this.displayProgressBar(percent * 4, fracs[0], timer, reverse ? lastStart : 0);
                this.displayProgressBar(0, fracs[1], timer, reverse ? lastStart - 1 : 0);
                this.displayProgressBar(0, fracs[2], timer, reverse ? lastStart - 2 : 0);
                this.displayProgressBar(0, fracs[3], timer, reverse ? lastStart - 3 : 0);
                break;
            case 1:
                this.displayProgressBar(100, fracs[0], timer, reverse ? lastStart : 0);
                this.displayProgressBar((percent - 25) * 4, fracs[1], timer, reverse ? lastStart - 1 : 1);
                this.displayProgressBar(0, fracs[2], timer, reverse ? lastStart - 2 : 0);
                this.displayProgressBar(0, fracs[3], timer, reverse ? lastStart - 3 : 0);
                break;
            case 2:
                this.displayProgressBar(100, fracs[0], timer, reverse ? lastStart : 0);
                this.displayProgressBar(100, fracs[1], timer, reverse ? lastStart - 1 : 1);
                this.displayProgressBar((percent - 50) * 4, fracs[2], timer, reverse ? lastStart - 2 : 2);
                this.displayProgressBar(0, fracs[3], timer, reverse ? lastStart - 3 : 3);
                break;
            case 3:
                this.displayProgressBar(100, fracs[0], timer, reverse ? lastStart : 0);
                this.displayProgressBar(100, fracs[1], timer, reverse ? lastStart - 1 : 1);
                this.displayProgressBar(100, fracs[2], timer, reverse ? lastStart - 2 : 2);
                this.displayProgressBar((percent - 75) * 4, fracs[3], timer, reverse ? lastStart - 3 : 3);
                break;
            case 4:
                this.displayProgressBar(100, fracs[0], timer, 0 - lastStart);
                this.displayProgressBar(100, fracs[1], timer, 1 - lastStart);
                this.displayProgressBar(100, fracs[2], timer, 2 - lastStart);
                this.displayProgressBar(100, fracs[3], timer, 3 - lastStart);
                break;
            default:
                this.displayProgressBar(0, fracs[0], timer, lastStart);
                this.displayProgressBar(0, fracs[1], timer, lastStart - 1);
                this.displayProgressBar(0, fracs[2], timer, lastStart - 2);
                this.displayProgressBar(0, fracs[3], timer, lastStart - 3);
                break;
        }

        this.elements.$icon[0].style.left = `${percent}%`;
    }

    bindEvents() {
        const units = document.querySelectorAll('.ssn__unit-wrapper');
        if (units.length === 0) {
            console.error('no units found on this page!');
            this.elements.$wrapper[0].style.display = 'none!important';
            return;
        }

        document.addEventListener(
            'ssn-module-update',
            this.updateProgress.bind(this)
        );

        this.updateProgress();
    }
}

jQuery( window ).on(
    'elementor/frontend/init',
    () => {
        const addHandler = ( $element ) => {
            elementorFrontend.elementsHandler.addHandler( SsnProgressBar, { $element } );
        };

        elementorFrontend.hooks.addAction( 'frontend/element_ready/ssn-progress-bar.default', addHandler );
    }
);