<?php
/**
 * Elementor Ssn Widgets.
 *
 * Ssn widget.
 *
 * @since 1.0.0
 */

namespace Ssn;
require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/wpml-ssn-cards-counter.php';

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

class SsnLoader implements \IWPML_Backend_Action, \IWPML_Frontend_Action
{
    private static $_instance = null;

    public static function instance()
    {
        if( is_null( self::$_instance ) )
        {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct()
    {
        add_action(
        		'elementor/frontend/after_register_scripts',
						[ $this, 'widget_scripts' ]
				);
        add_action(
        		'elementor/widgets/widgets_registered',
						[ $this, 'register_widgets' ], null
				);
				add_filter(
						'wpml_elementor_widgets_to_translate',
						[ $this, 'wpml_widgets_to_translate_filter' ]
				);
    }

		public function wpml_widgets_to_translate_filter( $widgets )
		{
				$widgets[ 'ssn-sub-module' ] = [
						'conditions' => [ 'widgetType' => 'ssn-sub-module' ],
						'fields'     => [
								[
										'field'       => 'module-slug',
										'type'        => esc_html__( 'Sub-module category', 'ssn' ),
										'editor_type' => 'LINE'
								],
								[
										'field'       => 'section-title',
										'type'        => esc_html__( 'Section title', 'ssn' ),
										'editor_type' => 'AREA'
								],
						],
				];

				$widgets[ 'ssn-counter' ] = [
						'conditions' => [ 'widgetType' => 'ssn-counter' ],
						'fields'     => [
								[
										'field'       => 'counter-label',
										'type'        => esc_html__( 'Counter label', 'ssn' ),
										'editor_type' => 'LINE'
								],
						],
						'integration-class' => 'WpmlSsnCounter'
				];

				return $widgets;
		}

		public function add_hooks() {
				add_action(
						'elementor/widgets/widgets_registered',
						[ $this, 'register_widgets' ]
				);
				add_filter(
						'wpml_custom_language_switcher_is_enabled',
						'__return_true'
				);
		}

    private function include_widget_files()
    {
        require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/ssn-sub-module-base.php';
        require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/ssn-sub-module-widget.php';
        require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/ssn-child-tag.php';
        require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/ssn-cards-counter-widget.php';
        require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/ssn-progress-bar.php';
        require_once PLUGIN_DIR . 'ext-templates/elementor/widgets/ssn-adult-tag.php';
    }

    public function register_widgets()
    {
        $this->include_widget_files();
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\SsnSubModuleWidget );
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\SsnCounter );
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\SsnProgressBar );
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\SsnChildCategory );
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\SsnAdultCategory );
    }

    public function widget_scripts()
    {
        wp_enqueue_style(
            'ssn-unit',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-unit.css',
            array(),
            '1.0.1'
        );

        wp_enqueue_style(
            'ssn-unit-header',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-unit-header.css',
            array(),
            '1.0.37'
        );

        wp_enqueue_style(
            'ssn-unit-elements',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-unit-elements.css',
            array(),
            '1.0.87'
        );

        wp_enqueue_style(
            'ssn-unit-content',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-unit-content.css',
            array(),
            '1.0.16'
        );

        wp_enqueue_style(
            'ssn-unit-cards',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-unit-cards.css',
            array(),
            '1.0.13'
        );

        wp_enqueue_style(
            'ssn-sub-modules-cards',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-sub-modules-cards.css',
            array(),
            '1.0.13'
        );

        wp_enqueue_style(
            'ssn-counter-style',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-counter.css',
            array(),
            '1.0.27'
        );

        wp_enqueue_style(
            'ssn-progress-bar-style',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-progress-bar.css',
            array(),
            '1.0.25'
        );

        wp_enqueue_style(
            'ssn-adult-tags-style',
            plugins_url() . '/ssn-modules/ext-templates/elementor/styles/ssn-adult-tag.css',
            array(),
            '1.0.16'
        );
    }
}

SsnLoader::instance();