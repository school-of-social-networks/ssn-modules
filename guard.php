<?php
/*
@package ssn-modules
*/

// Check for presence of wordpress globals, and block any attempt to access
// files from external source.
function check_wp_env() {
    if (
        !defined( 'ABSPATH' ) ||
        !function_exists( 'add_action' )
    ) {
        die( 'access forbidden' );
    }
}